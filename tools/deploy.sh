#!/bin/bash
echo "-------开始项目部署---------"
projectName="yingzi"
git_path="git@gitee.com:aspirin/letao.git"

datetime=$(date "+%Y%m%d-%H%M%S")
projectDeplayName=${projectName}${datetime}
cd /opt/source
echo "当前${projectName}项目Git地址:" $git_path
 echo "拉取${projectName}项目"
 sleep 1
 git clone ${git_path} ${projectDeplayName}
 sleep 1
 #cd ${projectDeplayName}
#if [ -n "$git_version" ];then
# echo "开始切换到指定${git_version}版本号"
# git checkout $git_version
# git show $git_version > info.txt
# cat info.txt
#else
# git show > info.txt
# cat info.txt
# echo "最新版本号"
#fi
#sleep 1
rm -rf ${projectDeplayName}/application/database.php
cp database.php ${projectDeplayName}/application/database.php
rm -f $projectName
ln -s $projectDeplayName $projectName
#time=$(date +"%Y-%m-%d")

echo "部署${projectName}项目成功！"